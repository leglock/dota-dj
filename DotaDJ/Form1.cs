﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DotaDJ.Properties;

namespace DotaDJ
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            var appName = Application.ProductName;
            var appVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            this.Text = appName + " v" + appVersion.Major + "." + appVersion.Minor;

            this.KeyPreview = true;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox box = new AboutBox();
            box.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var player = new System.Media.SoundPlayer(Resources.smb_pause);
            player.Play();
        }

        // Handle the KeyDown event to determine the type of character entered into the control. 
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            // Determine whether the keystroke is a number from the top of the keyboard. 
            if (e.KeyCode == Keys.F12 && (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                button1.PerformClick();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
